# Hugo+Asciidoc+ReST - a Docker image integrating Hugo, Asciidoc, and Restructured Text
# Copyright (C) 2022 "John Trammell <john.trammell@gmail.com>"
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

FROM alpine:3
MAINTAINER John Trammell <me@johntrammell.com>

RUN apk update && apk add bash && apk add hugo && apk add asciidoc && \
	apk add py-pygments && apk add sphinx && rm -rf /var/cache/apk/* && \
	apk add asciidoctor && apk add python3 && apk add py3-pip && \
	pip3 install docutils && mkdir /site-dir

WORKDIR /site-dir
VOLUME /site-dir

CMD ["hugo", "server", "--bind", "0.0.0.0"]
