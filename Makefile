
.PHONY: usage build bump

usage:
	@echo "usage: make [clean|build]"

build:
	docker build --tag trammell/hugo-adoc-rst:0.2 .
	docker tag trammell/hugo-adoc-rst:0.2 har

